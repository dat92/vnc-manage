#!/bin/bash
if [ -z $1 ]; then
	echo "Use:"
	echo "./myVNC.sh start"
	echo "./myVNC.sh stop"
	echo "./myVNC.sh restart"
	exit 1
fi

if [ "$1" == "restart" ] || [ "$1" == "stop" ]; then
	vncserver -kill :1
fi

if [ "$1" == "restart" ]; then
	sleep 5
fi

if [ "$1" == "restart" ] || [ "$1" == "start" ]; then
	vncserver :1 -geometry 1680x1050 -depth 16
fi
